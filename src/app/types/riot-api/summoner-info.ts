export interface SummonerInfo {
  id: string;
  accountId: string;
  puuid: string;
  name: string;
  profileIcon: number;
  revisionDate: number;
  summonerLevel: number;
}
