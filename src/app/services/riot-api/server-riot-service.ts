import axios, { AxiosResponse } from 'axios';
import { SummonerInfo } from "../../types/riot-api/summoner-info";

const token = 'RGAPI-151ad5c6-1323-45f1-a49c-f25e7c0e7efd';

const getRequest = (url: string) =>
  axios.get(url)
    .then((response) => {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });

export const getSummonerByName = async (summonerName: string)/*: Promise<SummonerInfo>*/ => {
  if (!!summonerName) {
    const url = `https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/${summonerName}?api_key=${token}`;
    // return new SummonerInfo(await getRequest(url));
    const response: SummonerInfo = await getRequest(url);
    return `
     The summoner ${response.name} is level ${response.summonerLevel}.
     His accountId is: ${response.accountId}.
    `;
  }
  return `usage: getSummonerByName [summonerName]`;
};
